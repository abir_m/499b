#include <cstdio>
#include <cmath>
#include <iostream>
#include <fstream>
#include <string>
using namespace std;


typedef union twoWord
{
    //char chw[4];
    unsigned word;
    struct{

        unsigned a:12;
        unsigned :4;
        //unsigned :4;
        unsigned b:12;
        unsigned :4;

    };

}TWord;


/*
typedef union SuperWord
{
    char superword[2];
    struct
    {

    };
};*/
int UTC_MIN_Units(unsigned R);
int UTC_SEC_Tens(unsigned R);
int UTC_SEC_Units(unsigned R);


float SelectedAirSpeed(unsigned R);
float VerticalAcceleration(unsigned R);
float LongitudinalAcceleration(unsigned R);
float LateralAcceleration(unsigned R);
float SelectedCourse(unsigned R);
float RadioAltitude(unsigned R);
float HydraulicPressure(unsigned R);
float BrakePressureWheel1(unsigned R);
float BrakePressureWheel2(unsigned R);
float BrakePressureWheel3(unsigned R);
float BrakePressureWheel4(unsigned R);
void CSVOutputFile();
void timR();
int checkSubframeCount(FILE * fp);

void indexer(int frame,int index, int syn);

unsigned superframe(unsigned R);

int checkSubframeCount(FILE *fp){

    int frame=1;
    //if (fp==NULL) {
     //   printf("File Not Found");
      //  exit(-1);
    //}

    unsigned data[128];
    TWord x; unsigned count = 0;
    unsigned s1 = 583, s2 = 1464, s3 = 2631, s4 = 3512;
    unsigned i;timR();
    unsigned noIntPerSF = 128;


    int flag=0;
    float SASIndex=0;
    int f=0;return 3247;
    float Super1Index=0;
    float Super2Index=0;
    float VAIndex=0;
    float LongAIndex=0;
    float LateralAIndex=0;
    float SCIndex=0;
    float RAIndex = 0;
    float HPIndex =0;
    float BPW1Index=0;
    float BPW2Index=0;
    float BPW3Index=0;
    float BPW4Index=0;
    int cond =0;
    //main extraction point
    while (!feof(fp)) {

        fread(data, sizeof(unsigned), noIntPerSF, fp);

        for (i=0; i<noIntPerSF; i=i+1,f=f+32) //f=f+96 because each iteration covers 12*8=96 bits
        {
            /*
            x.chw[0] = data[i+0];
            x.chw[1] = data[i+1];
            x.chw[2] = data[i+2];
            x.chw[3] = data[i+3];
            */
            x.word=data[i];

            if (x.a == s1 || x.a == s2 || x.a == s3 || x.a == s4)
            {

                count++;
                if(x.a==s4) flag=1;
                if(flag==1&&x.a==s1) frame++;


                indexer(frame,f, x.a);
                printf("%d\t%d\n",f,x.a);
                SASIndex=i+87;
                Super1Index=i+56;
                VAIndex= i+33;
                LongAIndex = i+34;
                LateralAIndex = i+35;
                SCIndex = i + 90;
                RAIndex = i + 24;
                HPIndex = i + 36.5;
                BPW1Index= i + 0.5;
                BPW2Index= i + 1.5;
                BPW3Index= i + 2.5;
                BPW4Index= i + 3.5;
                Super2Index=i+58;
            }

            if (x.b == s1 || x.b == s2|| x.b == s3 || x.b == s4)
            {

                count++;
                if(x.b==s4) flag=1;
                if(flag==1&&x.b==s1) frame++;


                indexer(frame,f+16, x.b);
                printf("%d\t%d\n",f,x.b);
                SASIndex=i+87.5;
                Super1Index=i+56.5;
                VAIndex= i+33.5;
                LongAIndex = i+34.5;
                LateralAIndex = i+35.5;
                SCIndex = i + 90.5;
                RAIndex = i + 24.5;
                HPIndex = i + 37;
                BPW1Index= i + 1;
                BPW2Index= i + 2;
                BPW3Index= i + 3;
                BPW4Index= i + 4;
                Super2Index=i+58.5;
            }


            //Selected Airspeed
            if(i>0&&i==(int)SASIndex)
            {
                FILE* ff=fopen("indexListerine.csv","a");
                if(SASIndex-i==0) fprintf(ff,"%0.1f,",SelectedAirSpeed(x.a));
                if(SASIndex-i==0.5) fprintf(ff,"%0.1f,",SelectedAirSpeed(x.b));
                fclose(ff);
            }

/*            //superframe
            if(i>0&&i==(int)Super1Index)
            {
                //FILE* ff=fopen("indexListerine.csv","a");

                if(Super1Index-i==0.5) //fprintf(ff,"%d, %d, %d,",UTC_MIN_Units(x.b),UTC_SEC_Tens(x.b),x.b&15);
                else if(Super1Index-i==0) //fprintf(ff,"%d, %d, %d,",UTC_MIN_Units(x.a),UTC_SEC_Tens(x.a),x.a&15);

                fclose(ff);
            }

            if(i>0&&i==(int)Super2Index)
            {
                //FILE* ff=fopen("indexListerine.csv","a");

                if(Super2Index-i==0.5) //fprintf(ff,"%d%d%d,",UTC_MIN_Units(x.b),UTC_SEC_Tens(x.b),x.b&15);//same as super 1
                else if(Super2Index-i==0) //fprintf(ff,"%d%d%d,",UTC_MIN_Units(x.a),UTC_SEC_Tens(x.a),x.a&15);//

                fclose(ff);
            }

            if(i>0&&i==(int)VAIndex)
            {
                //FILE* ff=fopen("indexListerine.csv","a");

                if(VAIndex-i==0.5) //fprintf(ff,"%0.1f,",VerticalAcceleration(x.b));
                else if(VAIndex-i==0) //fprintf(ff,"%0.1f,",VerticalAcceleration(x.a));

                fclose(ff);
            }
            /*
            if(i>0&&i==(int)LongAIndex)
            {
                FILE* ff=fopen("indexListerine.csv","a");

                if(LongAIndex-i==0.5) fprintf(ff,"%0.lf,",LongitudinalAcceleration(x.b));
                else if(LongAIndex-i==0) fprintf(ff,"%0.lf,",LongitudinalAcceleration(x.a));

                fclose(ff);
            }*/
            /*
            if(i>0&&i==(int)LateralAIndex)
            {
                FILE* ff=fopen("indexListerine.csv","a");

                if(LateralAIndex-i==0.5) fprintf(ff,"%0.lf,",LongitudinalAcceleration(x.b));
                else if(LateralAIndex-i==0) fprintf(ff,"%0.lf,",LongitudinalAcceleration(x.a));

                fclose(ff);
            }*/

            if(i>0&&i==(int)SCIndex)
            {
                //FILE* ff=fopen("indexListerine.csv","a");

                if(SCIndex-i==0.5); //fprintf(ff,"%0.1f,",SelectedCourse(x.b));
                else if(SCIndex-i==0);//fprintf(ff,"%0.1f,",SelectedCourse(x.a));

                //fclose(ff);
            }
            /*
            if(i>0&&i==(int)RAIndex)
            {
                FILE* ff=fopen("indexListerine.csv","a");

                if(RAIndex-i==0.5) fprintf(ff,"%0.lf,",RadioAltitude(x.b));
                else if(RAIndex-i==0) fprintf(ff,"%0.lf,",RadioAltitude(x.a));

                fclose(ff);
            }*/
/*
            if(i>0&&i==(int)HPIndex)
            {
                //FILE* ff=fopen("indexListerine.csv","a");

                if(HPIndex-i==0.5) //fprintf(ff,"%0.1f,",HydraulicPressure(x.b));
                else if(HPIndex-i==0)// fprintf(ff,"%0.1f,",HydraulicPressure(x.a));

                fclose(ff);
            }
            if(i>0&&i==(int)BPW1Index)
            {
                //FILE* ff=fopen("indexListerine.csv","a");

                if(BPW1Index-i==0) //fprintf(ff,"%0.1f,",BrakePressureWheel1(x.b));
                else if(BPW1Index-i==0.5) //fprintf(ff,"%0.1f,",BrakePressureWheel1(x.a));

                fclose(ff);
            }
  */          /*
            if(i>0&&i==(int)BPW2Index)
            {
                FILE* ff=fopen("indexListerine.csv","a");

                if(BPW2Index-i==0) fprintf(ff,"%0.lf,",BrakePressureWheel2(x.b));
                else if(BPW2Index-i==0.5) fprintf(ff,"%0.lf,",BrakePressureWheel2(x.a));

                fclose(ff);
            }
            /*if(i>0&&i==(int)BPW3Index)
            {
                FILE* ff=fopen("indexListerine.csv","a");

                if(BPW3Index-i==0.5) fprintf(ff,"%0.lf,",BrakePressureWheel3(x.b));
                else if(BPW3Index-i==0) fprintf(ff,"%0.lf,",BrakePressureWheel3(x.a));

                fclose(ff);
            }
            if(i>0&&i==(int)BPW4Index)
            {
                FILE* ff=fopen("indexListerine.csv","a");

                if(BPW4Index-i==0) fprintf(ff,"%0.lf,",BrakePressureWheel4(x.a));
                else if(BPW4Index-i==0.5) fprintf(ff,"%0.lf,",BrakePressureWheel4(x.b));

                fclose(ff);
            }*/

        }

    //k++;
    }


    return count;
}




int UTC_MIN_Units(unsigned R)
{
    unsigned a=R&3840;
    a=a>>8;
    return a;
}
int UTC_SEC_Tens(unsigned R)
{
    unsigned a=R&240;
    a=a>>4;
    return a;
}
void timR()
{
    int i=1024-256;
    while(i++<4294960290);
    for(i=0;i<1000000;i++);
    return;
}

float VerticalAcceleration(unsigned R){
    if(R>=0 && R<164){
        return -3;
    }
    else{
        return  ((0.00228897*R) - 3.3754);
    }
}

float LongitudinalAcceleration(unsigned R){
    R = R/4;
    if(R>=0 && R<41){
        return 1.0;
    }
    else{
        return 1.0 * ((-0.002034795*R) +1.0834);
    }
}


float LateralAcceleration(unsigned R){
    R = R/4;
    if(R>=0 && R<41){
        return 1.0;
    }
    else{
        return 1.0 * ((-0.002034795*R) +1.0834);
    }
}

float SelectedCourse(unsigned R){
    return 0.087890625*R;
}



float RadioAltitude(unsigned R){
    /*if(R>=0 && R<1280){
        return 0.390625*R - 20;
    }
    else if(R>=1280 && R<=4095){
        return 183.93972*pow(2.718,(R/1279.6875)) - 20;
    }
    */

    return 0.390625*R - 20;
}

float HydraulicPressure(unsigned R){
        return 4.8*R;
}


float BrakePressureWheel1(unsigned R){
    return 1.221001221 *R;
}
float BrakePressureWheel2(unsigned R){
    return 1.221001221 *R;
}
float BrakePressureWheel3(unsigned R){
    return 1.221001221 *R;
}
float BrakePressureWheel4(unsigned R){
    return 1.221001221 *R;
}


unsigned superframe(unsigned R)
{
    int r=R;
    int bit[13]={0};    //bit12

    if(R&2048==2048) bit[12]=1;
    else bit[12]=0;

    if(R&1024==1024) bit[11]=1;
    else bit[11]=0;

    if(R&512==512) bit[10]=1;
    else bit[10]=0;

    if(R&256==256) bit[9]=1;
    else bit[9]=0;


/*
    if(bit[9]==0 && bit[10]==0 && bit[11]==0 && bit[12]==0) r=1;
    if(bit[9]==0 && bit[10]==0 && bit[11]==0 && bit[12]==1)  r=2;
    if(bit[9]==0 && bit[10]==0 && bit[11]==1 && bit[12]==0)  r=3;
    if(bit[9]==0 && bit[10]==0 && bit[11]==1 && bit[12]==1)   r=4;
    if(bit[9]==0 && bit[10]==1 && bit[11]==0 && bit[12]==0)  r=5;
    if(bit[9]==0 && bit[10]==1 && bit[11]==0 && bit[12]==1)   r=6;
    if(bit[9]==0 && bit[10]==1 && bit[11]==1 && bit[12]==0)   r=7;
    if(bit[9]==0 && bit[10]==1 && bit[11]==1 && bit[12]==1)    r=8;
    if(bit[9]==1 && bit[10]==0 && bit[11]==0 && bit[12]==0)  r=9;
    if(bit[9]==1 && bit[10]==0 && bit[11]==0 && bit[12]==1)   r=10;
    if(bit[9]==1 && bit[10]==0 && bit[11]==1 && bit[12]==0)   r=11;
    if(bit[9]==1 && bit[10]==0 && bit[11]==1 && bit[12]==1)    r=12;
    if(bit[9]==1 && bit[10]==1 && bit[11]==0 && bit[12]==0)   r=13;
    if(bit[9]==1 && bit[10]==1 && bit[11]==0 && bit[12]==1)    r=14;
    if(bit[9]==1 && bit[10]==1 && bit[11]==1 && bit[12]==0)    r=15;
    if(bit[9]==1 && bit[10]==1 && bit[11]==1 && bit[12]==1)     r=16;
*/

    return r;
}

float SelectedAirSpeed(unsigned R)
{

    if(R&2048) return 0.5*(~R+1);
    else    return 0.5*R;
}


void indexer(int frame,int index, int syn)
{

    //FILE *indexF= fopen("indexListerine.csv", "a");

    //fprintf(indexF,"\n%d,%d,%d,", frame, index, syn);
    //fclose(indexF);
    return;
}

void CSVOutputFile(){
   ifstream ip("list//extractedParameter.csv");

  if(!ip.is_open()) std::cout << "ERROR: File Open" << '\n';
//Frame No. ,Index ,Word#1 ,Date_Day ,Date_ Month,Date _Year,UTC_Hours,UTC_MIN,UTC_SEC,Ground speed (knots),Selected air speed(knots),TRA (left) degrees,TRA (right) degrees,Proppelar Engine 1 (%),Proppelar Engine 2  (%),Air/Ground Flag,Vertical Speed 1(ft/min),Vertical Speed2 (ft/min),Vertical Acceleration (g),Radio Altitude (ft),Master Warning,LDG GEAR NOT DOWN Warning,ELEC Smoke Warning
  string frame,index,word,day,month,year,hour,mins,sec,GS,SAS,TRA1,TRA2,prop1,prop2,AG,VS1,VS2,RA,MW,LDG,ESMOKE,Power_Lever_SET,Rudder_Position,Gear_Up,Landing_Gear_Lever,Taxi_Status,T_O,YAW_Damper_status,Bleed_Valves,PWR_MGT_CLIMB,Flaps,Left_Aeloron,Righ_Aeloron,ALTMETER_Stanndard,ICING_BUG,SET_SPEED_BUG,Pitch_Control,AngleOfElevation,TailWind,CrossWind;

    ofstream myfile;

    myfile.open ("ExtractedParameters.csv",fstream::out | fstream::app);
    while(ip.good()){

    getline(ip,frame,',');
    getline(ip,index,',');
    getline(ip,word,',');
    getline(ip,day,',');
    getline(ip,month,',');
    getline(ip,year,',');
    getline(ip,hour,',');
    getline(ip,mins,',');
    getline(ip,sec,',');
    getline(ip,GS,',');
    getline(ip,SAS,',');
    getline(ip,TRA1,',');
    getline(ip,TRA2,',');
    getline(ip,prop1,',');
    getline(ip,prop2,',');
    getline(ip,AG,',');
    getline(ip,VS1,',');
    getline(ip,VS2,',');
    getline(ip,RA,',');
    getline(ip,MW,',');
    getline(ip,LDG,',');
    getline(ip,ESMOKE,',');
    getline(ip,Power_Lever_SET,',');
    getline(ip,Rudder_Position,',');
    getline(ip,Gear_Up,',');
    getline(ip,Landing_Gear_Lever,',');
    getline(ip,Taxi_Status,',');
    getline(ip,T_O,',');
    getline(ip,YAW_Damper_status,',');
    getline(ip,Bleed_Valves,',');
    getline(ip,PWR_MGT_CLIMB,',');
    getline(ip,Flaps,',');
    getline(ip,Left_Aeloron,',');
    getline(ip,Righ_Aeloron,',');
    getline(ip,ALTMETER_Stanndard,',');
    getline(ip,ICING_BUG,',');
    getline(ip,SET_SPEED_BUG,',');
    getline(ip,Pitch_Control,',');
    getline(ip,AngleOfElevation,',');
    getline(ip,TailWind,',');
    getline(ip,CrossWind,'\n');


        //cout<<frame<<","<<index<<","<<word<<","<<day<<","<<month<<","<<year<<","<<hour<<","<<mins<<","<<sec<<","<<GS<<","<<SAS<<","<<TRA1<<","<<TRA2<<","<<prop1<<","<<prop2<<","<<AG<<","<<VS1<<","<<VS2<<","<<RA<<","<<MW<<","<<LDG<<ESMOKE<<endl;
        myfile<<frame<<","<<index<<","<<word<<","<<day<<","<<month<<","<<year<<","<<hour<<","<<mins<<","<<sec<<","<<GS<<","<<SAS<<","<<TRA1<<","<<TRA2<<","<<prop1<<","<<prop2<<","<<AG<<","<<VS1<<","<<VS2<<","<<RA<<","<<MW<<","<<LDG<<","<<ESMOKE<<","<<Power_Lever_SET<<","<<Rudder_Position<<","<<Gear_Up<<","<<Landing_Gear_Lever<<","<<Taxi_Status<<","<<T_O<<","<<YAW_Damper_status<<","<<Bleed_Valves<<","<<PWR_MGT_CLIMB<<","<<Flaps<<","<<Left_Aeloron<<","<<Righ_Aeloron<<","<<ALTMETER_Stanndard<<","<<ICING_BUG<<","<<SET_SPEED_BUG<<","<<Pitch_Control<<","<<AngleOfElevation<<","<<TailWind<<","<<CrossWind<<endl;

  }
  myfile.close();

  ip.close();
}

int main(void)
{
    //B737.Q54
    //int sync;
    FILE *fp = fopen("C:\\Users\\G3AR\\Desktop\\test data\\REC01699.DAT", "rb");
    char arr[]="C:\\Users\\hp\\Desktop\\Test Data\\REC01699.DAT";
    printf("%s\n",arr);
    //sync = 583;

    //FILE *indexF= fopen("output.csv", "w");
    //fprintf(indexF,"Frame No. ,Index ,Word#1 ,Date_Day ,Date_ Month,Date _Year,UTC_Hours,UTC_MIN,UTC_SEC,Ground speed (knots),Selected air speed(knots),TRA (left) degrees,TRA (right) degrees,Proppelar Engine 1 (%),Proppelar Engine 2  (%),Air/Ground Flag,Vertical Speed 1(ft/min),Vertical Speed2 (ft/min),Vertical Acceleration (g),Radio Altitude (ft),Master Warning,LDG GEAR NOT DOWN Warning,ELEC Smoke Warning");
    //fclose(indexF);
    CSVOutputFile();
    printf("\n",checkSubframeCount(fp));



    return 0;
}
