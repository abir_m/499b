 <?php include_once "includes/header.php"; ?>

    <!-- Header -->
    <header class="masthead bg-primary text-white text-center">
      <div class="container">
        <img class="img-fluid mb-5 d-block mx-auto" src="img/prop.png" alt="home image" height="150px" width="150px"
        >

        <h1 class="text-uppercase mb-0">Welcome to <br />Air-Analitica</h1>
        <hr class="star-light">
        <h2 class="font-weight-light mb-0">EASY-FAST-RELIABLE</h2>

      </div>
    </header>


<?php include_once "includes/footer.php"; ?>
